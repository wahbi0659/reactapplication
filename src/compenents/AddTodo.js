import React, { useState } from 'react';

function AddTodo({ addTodo }) {
  const [text, setText] = useState('');

  const handleClick = () => {
    if (text.trim()) {
      addTodo(text);
      setText('');
    }
  };

  return (
    <>
      <input
        onClick={handleClick}
        type="text"
        value={text}
        onChange={(e) => setText(e.target.value)}
        placeholder="Add a new to-do"
      />
      <button type="submit">Add</button>
   </>
  );
}

export default AddTodo;
